using System.Linq;
using UserClass;
using EntityManager;
using System.IdentityModel.Tokens.Jwt;
using System;

namespace TokenClass
{
    public class TokenModel
    {
        public int Id {get; set;}
        public string _token {get; set;}

        public string InsertToken(UserModel User)
        {
            string token = TokenSpace.TokenHandler.PasswordToken(User.Id.ToString());
            this._token = token;
            return token;
        }

    }

    public class TokenObject
    {
        public string token {get; set;}

        public UserModel isToken(MysqlContext context, string token)
        {
            UserModel user = context.User
                .Where(u =>
                    u.token == token
                )
                .FirstOrDefault();
            return user;
        }

        public UserModel getUser(MysqlContext context)
        {
            var jwtHandler = new JwtSecurityTokenHandler();
            var tokenS = jwtHandler.ReadToken(this.token) as JwtSecurityToken;
            var id = tokenS.Claims.First(claim => claim.Type == "Id").Value;
            UserModel User = context.User
                .Where(u => 
                    u.Id.ToString() == id
                )
                .FirstOrDefault();
            return User;
        }

        
    }
}