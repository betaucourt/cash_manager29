using System;
using System.Linq;
using encryptorSpace;
using System.Net.Mail;
using EntityManager;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ProductClass;
using ListClass;
using Microsoft.EntityFrameworkCore;

namespace UserClass
{

    // Add profile data for application users by adding properties to the User class
    public class UserModel
    {
        [Key]
        public int Id { get; set; }
        public string userName {get; set;}
        public string email {get; set;}
        public string password {get; set;}
        public string token {get; set;}
        public int account { get; set; }
        public string role { get; set; }
        public ListModel shop { get; set; }

        public UserModel()
        {

        }

        public UserModel (UserObject user)
        {
            if (user.userName == null || user.email == null || user.password == null)
                return;
            userName = user.userName;
            email = user.email;
            role = UserObject.ROLES[0];
            password = Encryptor.EncryptString(user.password);

        }

        public void setPassword(string password)
        {
            this.password = Encryptor.EncryptString(password);
        }

    }


    
    public class UserObject
    {
        public int id {get; set;}
        public string userName {get; set;}
        public string email {get; set;}
        public string password {get; set;}
        public string token {get; set;}
        public string role { get; set; }
        public static readonly string[] ROLES = { "user", "admin" };

        public UserObject()
        {
            role = ROLES[0];
        }

        public UserModel CheckUser(MysqlContext context, string jwt_token)
        {
            return new UserModel();
        }

        public UserModel GetModifiedUser(MysqlContext context)
        {
            var User = context.User
                .Where( u => 
                    u.password == Encryptor.EncryptString(this.password) &&
                    u.Id == this.id
                )
                .FirstOrDefault();
            if (User == null || User.Id != this.id)
                return null;
            if (User.userName != null)
                User.userName = this.userName;
            if (User.email != null)
                User.email = this.email;
            return User;
        }

        public UserModel LogUser(MysqlContext context)
        {
            if (this.userName == null || this.password == null)
                return null;
            var user = context.User
                .Where(u =>
                    u.userName == this.userName &&
                    u.password == Encryptor.EncryptString(this.password)
                )
                .FirstOrDefault();
            return user;
        }

        public string RegisterUser()
        {
            if (this.password == null || this.userName == null || this.userName.Length < 4) {
                return "Missing password or userName length < 4";
            }
            try {
                MailAddress test = new MailAddress(this.email);
            }
            catch {
                return "Missing or invalid mail adress";
            }
            return null;
        }
        public UserObject (UserModel user)
        {
            if (user.userName == null || user.email == null || user.password == null)
                return;
            this.userName = user.userName;
            this.email = user.email;
            this.password = Encryptor.DecryptString(user.password);
        }

        public bool isGranted(UserModel User)
        {
            if (User.role == ROLES[1])
            {
                return true;
            } else {
                return false;
            }
        }
    }

}
