﻿using ProductClass;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using UserClass;

namespace ListClass
{
    public class ListModel
    {
        [Key]
        public int id { get; set; }
        public virtual List<ListProduct> Products { get; set; }
        public UserModel user { get; set; }

        public ListModel()
        {
        }




    }

    public class ListProduct
    {
        [Key]
        public int id { get; set; }
        public int ListModelId { get; set; }
        public virtual ListModel ListModel { get; set; }
        public int ProductId { get; set; }

        public virtual ProductModel product { get; set; }
        public int quantity { get; set; }
    }
}