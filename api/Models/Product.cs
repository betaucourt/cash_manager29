﻿using ListClass;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProductClass
{
    #region test
    public class ProductModel
    {
        [Key]
        public int id { get; set; }

        public string name { get; set; }

        public int price { get; set; }

        public int stock { get; set; }
        public int ListId { get; set; }
        public List<ListProduct> List { get; set; }
    }
    #endregion
}
