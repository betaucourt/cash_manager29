﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserClass;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AccountClass
{
    public class AccountModel
    {
        [Key]
        public int id { get; set; }
        public int user { get; set; }

        public int balance { get; set; }
    }
}
