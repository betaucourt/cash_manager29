using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore; // https://www.entityframeworktutorial.net/efcore/entity-framework-core-dbcontext.aspx
using Pomelo.EntityFrameworkCore.MySql.Infrastructure; // https://github.com/PomeloFoundation/Pomelo.EntityFrameworkCore.MySql/blob/master/README.md
using EntityManager;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.SwaggerUI;
using Swashbuckle.AspNetCore;
using System.Linq;

namespace api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors(c =>  
            {  
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());  
            });
            services.AddMvc().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });
            services.AddControllers().AddNewtonsoftJson();

            services.AddDbContextPool<MysqlContext>(options =>
                options.UseMySql(
                    "server=db;database=cash_manager;port=3306;userid=root;password=;",
                    mysqlOptions => {
                        mysqlOptions.ServerVersion(new Version(5, 7, 17), ServerType.MySql);
                    }
                ));

            services.AddDbContext<MysqlContext>(options =>
                    options.UseMySql(
                        "Server=(localdb)\\mssqllocaldb;Database=MySqlContext-5c121a90-040d-44b0-bc24-061ec2aab3ba;Trusted_Connection=True;MultipleActiveResultSets=true"                    )
            );
                    
            
            services.AddControllers();

            services.AddSwaggerDocument();
        }
   
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            
            
            app.UseCors(options => options.AllowAnyOrigin());  
            app.UseHttpsRedirection();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseRouting();

            app.UseAuthorization();
            
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<MysqlContext>())
                {
                    if (context.Database.EnsureCreated())
                        context.Database.Migrate();
                }
            }
        }
    }
}
