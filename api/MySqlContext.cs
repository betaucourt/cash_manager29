using Microsoft.EntityFrameworkCore;
using UserClass;
using TokenClass;
using ProductClass;
using AccountClass;
using ListClass;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Linq;

namespace EntityManager {
    public class MysqlContext : DbContext
    {
        public MysqlContext(DbContextOptions<MysqlContext> options)
            : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {   

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserModel>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd();
                entity.Property(e => e.userName).HasMaxLength(50);
                entity.HasIndex(e => e.userName).IsUnique();
                entity.Property(e => e.email).HasMaxLength(50);
                entity.HasOne(u => u.shop).WithOne(s => s.user).HasForeignKey<ListModel>(s => s.id).IsRequired();
            });


            modelBuilder.Entity<ListModel>(entity =>
            {
                entity.HasKey(l => l.id);
                entity.Property(l => l.id).ValueGeneratedOnAdd();
            });



            modelBuilder.Entity<TokenModel>(entity => 
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ProductModel>(entity =>
            {
                entity.HasKey(e => e.id);
                entity.Property(e => e.id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<AccountModel>(entity =>
            {
                entity.HasKey(e => e.id);
                entity.Property(e => e.id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ListProduct>(entity =>
            {
                entity.HasKey(e => e.id);
                entity.Property(e => e.id)
                    .ValueGeneratedOnAdd();
            });
            modelBuilder.Entity<ListProduct>().HasOne(lp => lp.ListModel).WithMany(l => l.Products).HasForeignKey(lp => lp.ListModelId);
            modelBuilder.Entity<ListProduct>().HasOne(lp => lp.product).WithMany(p => p.List).HasForeignKey(lp => lp.ProductId);
        }
        
        public DbSet<UserModel> User { get; set; }
        public DbSet<TokenModel> Token {get; set;}
        public DbSet<ProductModel> Product { get; set; }
        public DbSet<AccountClass.AccountModel> Account { get; set; }
        public DbSet<ListModel> List { get; set; }
        public DbSet<ListProduct> ListProduct { get; set; }
    }

}

