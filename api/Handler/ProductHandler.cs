﻿using EntityManager;
using ProductClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductHandler
{
    public class ProductObject
    {
        public string name;
        public int price;
        public int stock;

        public ProductObject()
        {

        }

        public ProductModel newProduct()
        {
            ProductModel newProduct = new ProductModel();
            Console.WriteLine(this.name);
            newProduct.name = this.name;
            newProduct.price = this.price;
            newProduct.stock = this.stock;
            return newProduct;
        }

        public ProductModel updateProduct(ProductModel toChange)
        {
            if (!string.IsNullOrEmpty(this.name))
                toChange.name = this.name;
            if (this.price != null)
                toChange.price = this.price;
            if (this.stock != null)
                toChange.stock = this.stock;
            return toChange;
        }

        public List<ProductModel> getAllProduct(MysqlContext context)
        {
            List<ProductModel> productList = context.Product.ToList();
            return productList;
        }

        public ProductModel getProductByName(MysqlContext context, string name)
        {
            ProductModel product = context.Product.Where(p =>
                p.name == name
            ).FirstOrDefault();
            return product;
        }

        public ProductModel getProductById(MysqlContext context, int id)
        {
            ProductModel product = context.Product.Where(p =>
                p.id == id
            ).FirstOrDefault();
            return product;
        }
    }
}
