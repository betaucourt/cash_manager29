﻿using AccountClass;
using EntityManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserClass;

namespace api.Handler
{
    public class AccountHandler
    {
        public int balance;
        public bool mod;

        public AccountHandler()
        {

        }

        public AccountModel getAccountByUser(MysqlContext context, UserModel User)
        {
            Console.WriteLine(User.account);
            int id = User.Id;
            AccountModel account = context.Account.Where(a =>
                    a.user == id
                ).FirstOrDefault();

            return account;
        }
    }
}
