﻿using EntityManager;
using ListClass;
using ProductClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserClass;

namespace api.Handler
{
    public class ShopHandler
    {
        public List<Product> products { get; set; }
        public List<ProductModel> DbProduct { get; set; }

        public bool fillProducts(MysqlContext context)
        {
            DbProduct = context.Product.Where(p => products
            .Select(product => product.id).Contains(p.id)).ToList();
            if (DbProduct.Count() != products.Count())
                return false;
            return true; ;
        }

        public void setList(MysqlContext context, UserModel User)
        {
            if (User.shop.Products.Count != 0)
                User.shop.Products.Clear();
            Console.WriteLine("bite");
            foreach(var product in products)
            {
                ProductModel tmp = DbProduct.Where(d => d.id == product.id).FirstOrDefault();
                Console.WriteLine(tmp.name);
                Console.WriteLine("bite");
                ListProduct listProduct = new ListProduct();
                listProduct.ListModelId = User.shop.id;
                listProduct.ProductId = tmp.id;
                listProduct.product = tmp;
                listProduct.ListModel = User.shop;
                listProduct.quantity = product.quantity;
                context.ListProduct.Add(listProduct);
                User.shop.Products.Add(listProduct);
                context.SaveChanges();
            }
        }
        
    }

    public class Product
    {
        public int id { get; set; }
        public int quantity { get; set; }
    }
}
