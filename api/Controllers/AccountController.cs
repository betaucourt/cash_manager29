﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductHandler;
using UserClass;
using EntityManager;
using TokenClass;
using AccountClass;
using ProductClass;
using Microsoft.EntityFrameworkCore;
using api.Handler;
using TokenSpace;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly MysqlContext _context;
        private readonly AccountHandler _account;


        public AccountController(MysqlContext context)
        {
            _context = context;
            _account = new AccountHandler();
        }

        /// <summary>
        /// Get Account from User /
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(UserModel), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get()
        {
            Console.WriteLine(Request.Headers["Authorization"]);
            UserModel User = TokenHandler.DecodeToken(Request.Headers["Authorization"], _context);
            if (User == null)
            {
                return BadRequest("Invalid Token");
            }
            AccountModel account = _account.getAccountByUser(_context, User);
            return Ok(account) ;
        }


        /// <summary>
        /// Get Account from User /
        /// </summary>
        [HttpPut]
        [ProducesResponseType(typeof(UserModel), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Put([FromBody]AccountHandler Account)
        {
            Console.WriteLine(Request.Headers["Authorization"]);
            UserModel User = TokenHandler.DecodeToken(Request.Headers["Authorization"], _context);
            if (User == null)
            {
                return BadRequest("Invalid Token");
            }
            AccountModel account = _account.getAccountByUser(_context, User);
            if (Account.mod)
            {
                account.balance += Account.balance;
            } else if (!Account.mod)
            {
                account.balance -= Account.balance;
            }
            _context.Update(account);
            _context.SaveChanges();
            return Ok(account);
        }
    }
}