﻿using api.Handler;
using EntityManager;
using ListClass;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProductClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokenSpace;
using UserClass;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShopController : ControllerBase
    {
        private readonly MysqlContext _context;
        private readonly AccountHandler _account;
        private readonly ShopHandler _shop;


        public ShopController(MysqlContext context)
        {
            _context = context;
            _account = new AccountHandler();
            _shop = new ShopHandler();
        }

        /// <summary>
        /// Get Account from User /
        /// </summary>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] ShopHandler shop)
        {
            UserModel User = TokenHandler.DecodeToken(Request.Headers["Authorization"], _context);
            if (User == null)
            {
                return BadRequest("Invalid Token");
            }
            if (!shop.fillProducts(_context))
                return BadRequest("Invalid item in list");
            shop.setList(_context, User);
            _context.Update(User);
            _context.SaveChanges();
            return Ok(User.shop);
        }

        /// <summary>
        /// Get Account from User /
        /// </summary>
        [HttpPatch]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Patch([FromBody] ShopHandler shop)
        {
            UserModel User = TokenHandler.DecodeToken(Request.Headers["Authorization"], _context);
            if (User == null)
            {
                return BadRequest("Invalid Token");
            }

            shop.fillProducts(_context);
            var List = _context.List.Find(User.shop.id);
            foreach(var item in shop.DbProduct)
            {
                var tmp = List.Products.Find(p => p.product.id == item.id);
                if (tmp == null)
                {
                    ListProduct product = new ListProduct();
                    product.product = item;
                    product.ProductId = item.id;
                    product.ListModel = List;
                    product.ListModelId = List.id;
                    product.quantity = shop.products.Where(p => p.id == item.id).Select(p => p.quantity).First();
                    List.Products.Add(product);
                    _context.Add(product);
                } else
                {
                    tmp.quantity += shop.products.Where(p => p.id == item.id).Select(p => p.quantity).First();
                    _context.Update(tmp);
                }
                
                _context.Update(List);
                _context.SaveChanges();

            }
            foreach(var tmp in List.Products)
            {
                tmp.ListModel = null;
                tmp.product.List = null;
            }
            return Ok(List.Products);
        }

            /// <summary>
            /// Get Account from User /
            /// </summary>
            [HttpDelete]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Delete([FromBody] ShopHandler shop)
        {
            UserModel User = TokenHandler.DecodeToken(Request.Headers["Authorization"], _context);
            if (User == null)
            {
                
                return BadRequest("Invalid Token");
            }
            ListProduct List = _context.ListProduct.Where(l =>
                l.ListModelId == User.shop.id
                ).FirstOrDefault();
            if (List == null)
            {
                return Ok();
            }
            foreach (var item in List.ListModel.Products)
            {
                item.product = _context.Product.Where(p =>
                    p.id == item.ProductId
                ).First();
                item.quantity -= shop.products.Where(p => p.id == item.product.id).Select(p => p.quantity).First();
                    
            }
            int deleted = List.ListModel.Products.RemoveAll(p => p.quantity <= 0);
            _context.Update<ListProduct>(List);
            _context.SaveChanges();
            return Ok(List.ListModel.Products);
        }

            /// <summary>
            /// Get Account from User /
            /// </summary>
            [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get([FromBody] ShopHandler shop)
        {
            UserModel User = TokenHandler.DecodeToken(Request.Headers["Authorization"], _context);
            if (User == null)
            {
                return BadRequest("Invalid Token");
            }

            ListProduct List = _context.ListProduct.Where(l =>
                l.ListModelId == User.shop.id)
                .FirstOrDefault();
            if (List == null)
                return Ok();
            foreach (var item in List.ListModel.Products)
            {
                item.product = _context.Product.Where(p =>
                    p.id == item.ProductId
                ).First();
            }

            return Ok(List.ListModel.Products.Select(p => p.product));
        }


    }
}
