using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UserClass;
using EntityManager;
using TokenClass;
using AccountClass;
using ListClass;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly MysqlContext _context;

        public UserController(MysqlContext context)
        { 
            _context = context;
        }

        /// <summary>
        /// Get User with token /
        /// parameters : userName, password
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(UserModel), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get([FromQuery]UserObject body)
        {
            UserModel user = body.LogUser(_context);
            if (user == null)
                return BadRequest("Bad credentials");
            user.token = TokenSpace.TokenHandler.getToken(user);
            _context.Update(user);
            try {
                _context.SaveChanges();
            }
            catch (DbUpdateException e) {
                return BadRequest(e.InnerException.Message);
            }
            user.password = null;
            return Ok(user);
        }


        /// <summary>
        /// Create new User /
        /// Parameter: userName, password, email
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(UserModel), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody]UserObject body)
        {
            string response;
            if ((response = body.RegisterUser()) != null)
                return BadRequest(response);
            UserModel user = new UserModel(body);
            AccountModel account = new AccountModel();
            ListModel list = new ListModel();
            _context.User.Add(user);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                _context.Remove(user);
                return BadRequest(e.InnerException.Message);
            }
            _context.Account.Add(account);
            _context.List.Add(list);
            _context.SaveChanges();
            user.account = account.id;
            user.shop = list;
            _context.Update(account);
            _context.Update(user);
            _context.SaveChanges();
            user.password = null;
            return Ok(user);
        }

        /// <summary>
        /// Delete User /
        /// Parameter : userName, password
        /// </summary>
        [HttpDelete]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> Delete([FromBody]UserObject Body)
        {
            UserModel User = Body.LogUser(_context);
            
            if (User == null)
                return BadRequest();
            _context.Remove(User);
            try {
                _context.SaveChanges();
            }
            catch (DbUpdateException e) {
                return BadRequest(e.InnerException.Message);
            }
            return Ok();
        }

        /// <summary>
        /// Modify User /
        /// Parameter : id, password
        /// </summary>
        [HttpPatch]
        [ProducesResponseType(typeof(UserModel), 200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> Patch([FromBody]UserObject Body)
        {
            UserModel User = Body.GetModifiedUser(_context);
            if (User == null)
                return BadRequest("Invalid id/password combination");
            _context.Update(User);
            try {
                _context.SaveChanges();
            }
            catch (DbUpdateException e) {
                return BadRequest(e.InnerException.Message);
            }
            User.password = null;
            return Ok(User);

        }

        /// <summary>
        /// Ask password token /
        /// Parameter : userName, password
        /// </summary>
        [Route("Password")]
        [HttpPost]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> AskPassword([FromBody]UserObject Body)
        {
            UserModel user = Body.LogUser(_context);
            if (user == null)
            {
                return BadRequest("Invalid token");
            }
            TokenModel token = new TokenModel();
            token.InsertToken(user);
            _context.Add(token);
            try {
                _context.SaveChanges();
            }
            catch (DbUpdateException e) {
                return BadRequest(e.InnerException.Message);
            }
            return Ok(token._token);
        }

        /// <summary>
        /// Change User password /
        /// Parameter : token, password
        /// </summary>
        [Route("Password")]
        [HttpPatch]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> ChangePassword([FromQuery]TokenObject Query, [FromBody]UserObject password)
        {
            UserModel User = null;
            if ((User = Query.isToken(_context, Request.Headers["Authorization"])) == null) {
                User = Query.getUser(_context);
                if (User == null)
                    return BadRequest("Token not found");
                User.setPassword(password.password);
                _context.Update(User);
                try {
                    _context.SaveChanges();
                }
                catch (DbUpdateException e) {
                    return BadRequest(e.InnerException.Message);
                }
                return Ok();
            }
            return BadRequest("Token not found");
        }

        
    }

}
