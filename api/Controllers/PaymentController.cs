﻿
using AccountClass;
using EntityManager;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TokenClass;
using TokenSpace;
using UserClass;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly MysqlContext _context;

        public PaymentController(MysqlContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get User with token /
        /// parameters : userName, password
        /// </summary>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] TokenObject token)
        {
            UserModel User = TokenHandler.DecodeToken(Request.Headers["Authorization"], _context);
            if (User == null)
            {
                return BadRequest("Invalid Token");
            }
            TokenModel BdToken = new TokenModel();
            if (token.token.Length == 0)
            {
                token.token = BdToken._token = BdToken.InsertToken(User);
                _context.Add(BdToken);
                _context.SaveChanges();
                return Ok(token.token);
            } else {
                var isToken = _context.Token.Where(t => t._token == token.token).FirstOrDefault();
                if (isToken == null)
                {
                    return BadRequest("Invalid token, try again to get new one");
                }
                AccountModel account = _context.Account.Where(a => a.id == User.account).FirstOrDefault();
                var ProductList = _context.ListProduct.Where(p => p.ListModelId == User.shop.id).Include(p => p.product).ToList();
                if (ProductList.Count == 0)
                {
                    return BadRequest("No product selected");
                }
                float price = 0;
                foreach (var product in ProductList)
                {
                    price += product.product.price * product.quantity;
                }
                if (price > account.balance)
                {
                    _context.Remove(isToken);
                    _context.SaveChanges();
                    return BadRequest("Insufficient fund");
                } else {
                    account.balance -= (int)price;
                }
                foreach(var product in ProductList)
                {
                    _context.Remove(product);
                }
                _context.Update(account);
                _context.Remove(isToken);
                _context.SaveChanges();
                return Ok(account.balance);
            }
        }
    }
}