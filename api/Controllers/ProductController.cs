﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductHandler;
using UserClass;
using EntityManager;
using TokenClass;
using AccountClass;
using ProductClass;
using Microsoft.EntityFrameworkCore;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly MysqlContext _context;
        private readonly ProductObject _product;


        public ProductController(MysqlContext context)
        {
            _context = context;
            _product = new ProductObject();
        }

        /// <summary>
        /// Create Product
        /// Bearer_token
        /// Body: {name, price, stock}
        /// </summary>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> Post([FromQuery] TokenObject Query, [FromBody] ProductObject product)
        {
            ProductModel newProduct = product.newProduct();
            _context.Product.Add(newProduct);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                if (e.InnerException.Message.Contains("Duplicate"))
                    return BadRequest(e.InnerException.Message);
                _context.Remove(newProduct);
            }
            return Ok(newProduct);
        }


        /// <summary>
        /// Create Product
        /// Bearer_token
        /// Body: {name, price, stock}
        /// </summary>
        [HttpDelete]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> Delete(int id)
        {
            if (id == null)
                return BadRequest("Invalid Id");
            ProductModel toModify = _product.getProductById(_context, id);
            if (toModify == null)
                return BadRequest("Invalid Id");
            _context.Remove(toModify);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                return BadRequest(e.InnerException.Message);
            }
            return Ok();
        }

            /// <summary>
            /// Create Product
            /// Bearer_token
            /// Body: {name, price, stock}
            /// </summary>
            [HttpPatch]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> Patch(int id, [FromBody] ProductObject product)
        {
            if (id == null)
                return BadRequest("Invalid Id");
            ProductModel toModify = product.getProductById(_context, id);
            if (toModify == null)
                return BadRequest("Invalid Id");
            toModify = product.updateProduct(toModify);
            _context.Update(toModify);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                return BadRequest(e.InnerException.Message);
            }
            return Ok(toModify);
        }

        /// <summary>
        /// Create Product
        /// Bearer_token
        /// Body: {name, price, stock}
        /// </summary>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> Get(string name)
        {
            if (name == null)
            {
                List<ProductModel> productList = _product.getAllProduct(_context);
                return Ok(productList);
            } else
            {
                ProductModel ProductSingle = _product.getProductByName(_context, name);
                if (ProductSingle == null )
                    return BadRequest("Invalid product name or no product in Database");
                return Ok(ProductSingle);
            }
        }
    }
}
