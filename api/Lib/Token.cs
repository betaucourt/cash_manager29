using Microsoft.IdentityModel.Tokens;
using System.Linq;

using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using EntityManager;
using System;
using System.Text;
using UserClass;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace TokenSpace {
    static class TokenHandler {
        
        public const string security = "QeThWmZq4t7w!z%C";
        public const string url = "EpiKodi";

        
        public static string getToken(UserModel user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(security));    
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);    
  
            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();    
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));    
            permClaims.Add(new Claim("Id", user.Id.ToString()));
  
            //Create Security Token object by giving required parameters    
            var token = new JwtSecurityToken(null, //Issure    
                    null,  //Audience    
                    permClaims,    
                    expires: DateTime.Now.AddDays(1),    
                    signingCredentials: credentials);    
            string jwt_token = new JwtSecurityTokenHandler().WriteToken(token);    
            Console.WriteLine(jwt_token);
            return jwt_token;
        }

        public static string PasswordToken(string Id)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(security));    
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));    
            permClaims.Add(new Claim("Id", Id));
            permClaims.Add(new Claim("Reason", "password"));
            var token = new JwtSecurityToken(null, //Issure    
                    null,  //Audience    
                    permClaims,    
                    expires: DateTime.Now.AddMinutes(5), 
                    signingCredentials: credentials);    
            string jwt_token = new JwtSecurityTokenHandler().WriteToken(token);    
            Console.WriteLine(jwt_token);
            return jwt_token;
        }

        public static UserModel DecodeToken(string token, MysqlContext context)
        {
            token = token.Replace("Bearer ", "");
            Console.WriteLine(token);
            var tokenHandler = new JwtSecurityTokenHandler();
            JwtSecurityToken securityToken;
	        try {
                securityToken = tokenHandler.ReadToken(token) as JwtSecurityToken;
            }
            catch {
                return null;
            }
            var IdClaim = securityToken.Claims.First(claim => claim.Type == "Id").Value;
            return context.User
                .Where( u => 
                    u.Id.ToString() == IdClaim
                )
                .Include(u => u.shop)
                .Include(u => u.shop.Products)
                .FirstOrDefault();
        }

        public static UserModel decryptHeader(string header, MysqlContext context)
        {
            if (header == null || header!.StartsWith("bearer"))
                return null;
            Console.WriteLine("test");
            string token = header.Substring("bearer".Length).Trim();
            return DecodeToken(token, context);
        }
    }
}