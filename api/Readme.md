Setup cli (fedora):
https://dotnet.microsoft.com/download/dotnet-core/3.1

database: Mysql

configuration: cp ./appsettings.json.example appsettings.json
l10: DefaultConnection

.Net Cli: `dotnet add package Pomelo.EntityFrameworkCore.MySql --version 3.2.4`

setup:
`dotnet ef migrations add FileName`
`dotnet ef database update`

run:
`dotnet run`
With watcher:
`dotnet watch run`

Media:
stored on project/api/media/User-Id/
resume.csv contains files stored in subfolders

serv default port : 
https - 5001
http - 5000

Doc api : https://127.0.0.1:5001/swagger