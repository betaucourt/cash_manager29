import 'package:flutter/material.dart';
import 'package:flutter_app/components/bottom_navigation.dart';
import 'package:flutter_app/components/cartCounter.dart';
import 'package:flutter_app/components/sizePicker.dart';
import 'package:flutter_app/models/cartItem_model.dart';
import 'package:flutter_app/models/product_model.dart';

class BottomDetails extends StatefulWidget {
  final Product product;
  final Function(CartItem) addItem;

  const BottomDetails({Key key, this.product, this.addItem}) : super(key: key);

  @override
  _BottomDetailsState createState() => _BottomDetailsState(product, addItem);
}

class _BottomDetailsState extends State<BottomDetails> {
  final Product product;
  final Function(CartItem) addItem;
  int productQty = 1;
  String size = '42';
  addQty() {
    setState(() {
      productQty++;
    });
  }

  removeQty() {
    setState(() {
      productQty--;
    });
  }

  changeSize(newSize) {
    setState(() {
      size = newSize;
    });
  }

  _BottomDetailsState(this.product, this.addItem);

  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.3),
      padding: EdgeInsets.symmetric(horizontal: 20),
      // height: 500,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24),
          topRight: Radius.circular(24),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          SizePicker(changeSize: changeSize),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Text(
              product.description,
              style: TextStyle(height: 1.5),
              textAlign: TextAlign.justify,
            ),
          ),
          CartCounter(addQty: addQty, removeQty: removeQty),
          SizedBox(
            width: double.infinity,
            child: FlatButton(
              onPressed: () {
                CartItem item = new CartItem(
                    product: product,
                    quantity: productQty,
                    size: size,
                    price: product.price * productQty);
                addItem(item);
                Navigator.pop(context);
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18)),
              color: product.color,
              child: Text(
                "Add to Cart".toUpperCase(),
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DetailsScreen extends StatelessWidget {
  final Product product;
  final Function(CartItem) addItem;

  const DetailsScreen({this.product, this.addItem});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: product.color,
      appBar: AppBar(
        backgroundColor: product.color,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_arrow_left_rounded,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.white,
            ),
            onPressed: () {},
          ),
          SizedBox(width: 10.0)
        ],
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        product.category.toUpperCase(),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12.5,
                        ),
                      ),
                      Text(
                        product.title,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(text: "Price\n"),
                                TextSpan(
                                  text: "\$${product.price}",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline4
                                      .copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Hero(
                              tag: "${product.id}",
                              child: Image.asset(
                                product.image,
                                fit: BoxFit.fill,
                              ),
                            ),
                          )
                        ],
                      )
                    ]),
              ),
            ),
            Expanded(
              child: BottomDetails(product: product, addItem: addItem),
            )
          ],
        ),
        // )
        // ),
      ),
      bottomNavigationBar: BottomNav(),
    );
  }
}
