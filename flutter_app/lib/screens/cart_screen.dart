import 'package:flutter_app/components/bottom_navigation.dart';
import 'package:flutter_app/components/cartBody.dart';
import 'package:flutter_app/models/cart_model.dart';
import 'package:flutter/material.dart';

class CartScreen extends StatelessWidget {
  final Cart cart;
  final Function(int) removeItem;

  const CartScreen({Key key, this.cart, this.removeItem}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.keyboard_arrow_left_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [        Text("Total : \$${cart.totalPrice}", style: TextStyle(fontSize: 20),)
              ],
            ),
            SizedBox(width: 10.0),
          ],
        ),
        bottomNavigationBar: BottomNav(),
        body: SafeArea(child:
              CartBody(cart, removeItem),
        )
    );
  }
}
