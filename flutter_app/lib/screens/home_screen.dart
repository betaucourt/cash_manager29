// import 'package:flutter/material.dart';
// import 'package:flutter_app/models/cartItem_model.dart';
//
// class HomeScreen2 extends StatefulWidget {
//   @override
//   _HomeScreen2State createState() => _HomeScreen2State();
// }
//
// class _HomeScreen2State extends State<HomeScreen2> {
//   // Widget _buildItem(int index){
//   //   return Container(
//   //     height: 250,
//   //     color: Colors.blueGrey,
//   //     child:
//   //
//   //   );
//   // }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         backgroundColor: Colors.grey,
//         body: SafeArea(
//             child: ListView.builder(
//                 itemCount: cartItemsList.length,
//                 itemBuilder: (BuildContext context, int index) {
//                   CartItem cartItem = cartItemsList[index];
//                   return Container(
//                       margin: EdgeInsets.all(10.0),
//                       height: 200,
//                       decoration: BoxDecoration(
//                           color: Colors.blueGrey,
//                           borderRadius: BorderRadius.circular(20.0),
//                           boxShadow: [
//                             BoxShadow(
//                                 color: Colors.black,
//                                 offset: Offset(0.0, 2.0),
//                                 blurRadius: 6.0)
//                           ]),
//                       child: Stack(
//                         children: [
//                           Positioned(
//                             right: 0.0,
//                             child: Container(
//                                 height: 200,
//                                 width: 200,
//                                 decoration: BoxDecoration(
//                                     borderRadius: BorderRadius.circular(10.0)),
//                                 child: Padding(
//                                   padding: const EdgeInsets.all(15.0),
//                                   child: Column(
//                                     crossAxisAlignment:
//                                         CrossAxisAlignment.start,
//                                     mainAxisAlignment:
//                                         MainAxisAlignment.spaceBetween,
//                                     children: [
//                                       Text(cartItem.name,
//                                           style: TextStyle(
//                                             fontSize: 22,
//                                             fontWeight: FontWeight.bold,
//                                           )),
//                                       Text(cartItem.description),
//                                       Text('qty: ${cartItem.quantity}'),
//                                       Text('Price: ${cartItem.totalPrice}')
//                                     ],
//                                   ),
//                                 )),
//                           ),
//                           Container(
//                               decoration: BoxDecoration(
//                                   color: Colors.white,
//                                   borderRadius: BorderRadius.circular(20.0),
//                                   boxShadow: [
//                                     BoxShadow(
//                                         color: Colors.black,
//                                         offset: Offset(0.0, 2.0),
//                                         blurRadius: 6.0)
//                                   ]),
//                               child: ClipRRect(
//                                   borderRadius: BorderRadius.circular(20.0),
//                                   child: Image.network(
//                                     cartItem.imageUrl,
//                                     height: 200,
//                                     width: 175,
//                                     fit: BoxFit.cover,
//                                   )))
//                         ],
//                       ));
//                 }
//                 )
//         )
//     );
//   }
// }
