// import 'package:flutter/material.dart';
// import 'cardItem.dart';
// import 'package:flutter_app/models/product_model.dart';
// import 'details_screen.dart';
//
// class HomeScreen extends StatefulWidget {
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }
//
// class _HomeScreenState extends State<HomeScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: Column(
//           children: [
//             Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 20.0),
//               child: Text(
//                 "Women",
//                 style: Theme.of(context)
//                     .textTheme
//                     .headline5
//                     .copyWith(fontWeight: FontWeight.bold),
//               ),
//             ),
//             Expanded(
//               child: Padding(
//                 padding: EdgeInsets.symmetric(horizontal: 20.0),
//                 child: GridView.builder(
//                     itemCount: products.length,
//                     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                       crossAxisCount: 2,
//                       mainAxisSpacing: 20.0,
//                       crossAxisSpacing: 20.0,
//                       childAspectRatio: 0.75,
//                     ),
//                     itemBuilder: (context, index) {
//                       return GestureDetector(
//                           onTap: () => Navigator.push(
//                               context,
//                               MaterialPageRoute(
//                                 builder: (context) => DetailsScreen(
//                                   product: products[index],
//                                 ),
//                               )),
//                           child: CardItem(
//                             product: products[index],
//                           ));
//                     }),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/models/cart_model.dart';
import 'package:flutter_app/models/product_model.dart';
import 'package:flutter_app/screens/cart_screen.dart';
import 'details_screen.dart';
import 'package:flutter_app/components/bottom_navigation.dart';
import 'package:flutter_app/models/cartItem_model.dart';

class ShopScreen extends StatefulWidget {
  @override
  _ShopScreenState createState() => _ShopScreenState();
}

class _ShopScreenState extends State<ShopScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  PageController _pageController;

  Cart cart = Cart();
  int cartQuantity = 0;

  void onAddItem(item) {
    setState(() {
      cartQuantity++;
    });
    print(
        'ADD \n${item.product.title}\n${item.quantity}\n${item.price}\n${item.size}');
    cart.addItem(item);
  }

  void onRemoveItem(item) {
    setState(() {
      cartQuantity--;
    });
    cart.removeItem(item);
  }

  void onUpdateQty(item, qty) {
    cart.updateQty(item, qty);
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: 5, vsync: this);
    _pageController = PageController(initialPage: 0, viewportFraction: 0.8);
  }

  _itemSelector(int index) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (BuildContext context, Widget widget) {
        double value = 1;
        if (_pageController.position.haveDimensions) {
          value = _pageController.page - index;
          value = (1 - (value.abs() * 0.3)).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 500.0,
            width: Curves.easeInOut.transform(value) * 400.0,
            child: widget,
          ),
        );
      },
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) =>
                  DetailsScreen(product: products[index], addItem: onAddItem),
            ),
          );
        },
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: products[index].color,
                borderRadius: BorderRadius.circular(20.0),
              ),
              margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 30.0),
              child: Stack(
                children: <Widget>[
                  Center(
                    child: Hero(
                      tag: products[index].image,
                      child: Image(
                        height: 280.0,
                        width: 280.0,
                        image: AssetImage(
                          products[index].image,
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 30.0,
                    right: 30.0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'FROM',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                          ),
                        ),
                        Text(
                          '\$${products[index].price}',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 25.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    bottom: 40.0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          products[index].category.toUpperCase(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12.5,
                          ),
                        ),
                        SizedBox(height: 5.0),
                        Text(
                          products[index].title,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 4.0,
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(
                      color: products[index].color,
                      style: BorderStyle.solid,
                      width: 1.0),
                  borderRadius: BorderRadius.circular(90.0),
                  color: Colors.white,
                ),
                child: RawMaterialButton(
                  child: Icon(
                    Icons.add_shopping_cart,
                    color: products[index].color,
                    size: 30.0,
                  ),
                  onPressed: () {
                    CartItem item = new CartItem(
                        product: products[index],
                        quantity: 1,
                        size: '42',
                        price: products[index].price);
                    onAddItem(item);
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Padding(
  // padding: EdgeInsets.symmetric(horizontal: 30.0),
  // child: Row(
  // mainAxisAlignment: MainAxisAlignment.end,
  // children: <Widget>[
  // Icon(
  // Icons.search,
  // size: 30.0,
  // color: Colors.black,
  // ),
  // SizedBox(width: 20.0),
  // Icon(
  // Icons.shopping_cart,
  // size: 30.0,
  // color: Colors.black,
  // ),
  // ],
  // ),
  // ),
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.home,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
        actions: <Widget>[
          // Icon(
          //   Icons.search,
          //   size: 30.0,
          //   color: Colors.red,
          // ),
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.black,
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: Stack(
              children: <Widget>[
                Icon(Icons.shopping_cart, color: Colors.black),
                if (cartQuantity > 0)
                  Positioned(
                    top: 0.0,
                    right: -2,
                    child: Stack(
                      children: <Widget>[
                        Icon(Icons.brightness_1, size: 18.0, color: Colors.red),
                        Positioned(
                          top: 3.0,
                          right: (cartQuantity < 10 ? 5.0 : 3.0),
                          child: new Text('$cartQuantity',
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontSize: 10.0,
                                  fontWeight: FontWeight.w500)),
                        )
                      ],
                    ),
                  )
              ],
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => CartScreen(
                    cart: cart,
                  ),
                ),
              );
            },
          ),
          SizedBox(width: 10)
        ],
      ),
      bottomNavigationBar: BottomNav(),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: SafeArea(
          child: Center(
           child: Container(
              height: 500,
              width: double.infinity,
              /*decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: Colors.green),*/
               child: PageView.builder(
                 controller: _pageController,
                 itemCount: products.length,
                 itemBuilder: (BuildContext context, int index) {
                   return _itemSelector(index);
                 },
               ),
            ),
          ),
        ),
      ),
    );
  }
}
