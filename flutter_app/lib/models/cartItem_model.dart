import 'package:flutter_app/models/product_model.dart';
import 'package:flutter/material.dart';

class CartItem {
  String id = UniqueKey().toString();
  Product product;
  int quantity;
  String size;
  double price;

  CartItem({this.product, this.size, this.quantity, this.price});
}
//
// List<CartItem> cartItemsList = [
//   CartItem(
//       imageUrl: 'https://picsum.photos/200/300',
//       name: 'Pantalon Noir',
//       quantity: 2,
//       totalPrice: 50.0,
//       description: 'Pantalon Noir de la marque Nike'),
//   CartItem(
//       imageUrl: 'https://picsum.photos/200/300',
//       name: 'Sweat Large',
//       quantity: 2,
//       totalPrice: 50.0,
//       description: 'Sweat Large Bleu'),
//   CartItem(
//       imageUrl: 'https://picsum.photos/200/300',
//       name: 'Short de bain',
//       quantity: 2,
//       totalPrice: 50.0,
//       description: 'Short de bain à fleurs'),
//   CartItem(
//       imageUrl: 'https://picsum.photos/200/300',
//       name: 'Chaussures',
//       quantity: 2,
//       totalPrice: 50.0,
//       description: 'Chaussures adidas'),
// ];
