import 'package:flutter/material.dart';
import 'package:flutter_app/constants.dart';

class Product {
  final String image, title, description, category;
  final int size, id;
  final double price;
  final Color color;

  Product({
    this.id,
    this.image,
    this.title,
    this.price,
    this.description,
    this.category,
    this.size,
    this.color,
  });
}

List<Product> products = [
  Product(
      id: 0,
      title: "Jordan 4 Metallic Red",
      price: 234,
      size: 12,
      description: dummyText,
      category: 'Sneakers',
      image: "assets/images/metallic_red.png",
      color: metallicRed),
  Product(
      id: 1,
      title: "Jordan 4 Metallic Orange",
      price: 234,
      size: 8,
      description: dummyText,
      category: 'Sneakers',
      image: "assets/images/metallic_orange.png",
      color: metallicOrange),
  Product(
      id: 2,
      title: "Jordan 4 Metallic Purple",
      price: 234,
      size: 10,
      description: dummyText,
      category: 'Sneakers',
      image: "assets/images/metallic_purple.png",
      color: metallicPurple),
  Product(
      id: 3,
      title: "Jordan 4 Metallic Green",
      price: 234,
      size: 11,
      description: dummyText,
      category: 'Sneakers',
      image: "assets/images/metallic_green.png",
      color: metallicGreen),
  // Product(
  //     id: 4,
  //     title: "Office Code",
  //     price: 234,
  //     size: 12,
  //     description: dummyText,
  //     category: 'Sneakers',
  //     image: "assets/images/metallic_green.png",
  //     color: metallicGreen),
  // Product(
  //   id: 5,
  //   title: "Office Code",
  //   price: 234,
  //   size: 12,
  //   description: dummyText,
  //   category: 'Sneakers',
  //   image: "assets/images/metallic_green.png",
  //   color: metallicGreen,
  // ),
];

String dummyText =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. When an unknown printer took a galley.";
