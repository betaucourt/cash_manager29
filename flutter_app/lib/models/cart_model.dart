import 'package:flutter_app/models/cartItem_model.dart';

class Cart {
  List<CartItem> items = [];
  double totalPrice = 0;

  void addItem(CartItem item) {
    items.add(item);
    totalPrice += item.price;
  }

  void removeItem(int index) {
    print(items);
    CartItem item = items[index];
    items.remove(item);
    totalPrice -= item.price;
  }

  void updateQty(CartItem item, int qty) {
    items[items.indexOf(item)].quantity = qty;
  }

  int getQty() {
    return items.length;
  }
}
//
// List<CartItem> cartItemsList = [
//   CartItem(
//       imageUrl: 'https://picsum.photos/200/300',
//       name: 'Pantalon Noir',
//       quantity: 2,
//       totalPrice: 50.0,
//       description: 'Pantalon Noir de la marque Nike'),
//   CartItem(
//       imageUrl: 'https://picsum.photos/200/300',
//       name: 'Sweat Large',
//       quantity: 2,
//       totalPrice: 50.0,
//       description: 'Sweat Large Bleu'),
//   CartItem(
//       imageUrl: 'https://picsum.photos/200/300',
//       name: 'Short de bain',
//       quantity: 2,
//       totalPrice: 50.0,
//       description: 'Short de bain à fleurs'),
//   CartItem(
//       imageUrl: 'https://picsum.photos/200/300',
//       name: 'Chaussures',
//       quantity: 2,
//       totalPrice: 50.0,
//       description: 'Chaussures adidas'),
// ];
