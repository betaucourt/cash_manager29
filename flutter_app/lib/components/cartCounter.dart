import 'package:flutter/material.dart';

class CartCounter extends StatefulWidget {
  final Function() addQty;
  final Function() removeQty;

  const CartCounter({Key key, this.addQty, this.removeQty}) : super(key: key);

  @override
  _CartCounterState createState() => _CartCounterState(addQty, removeQty);
}

class _CartCounterState extends State<CartCounter> {
  int nbItems = 1;

  _CartCounterState(Function() addQty, Function() removeQty);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          'Quantity',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Row(
          children: [
            GestureDetector(
              onTap: () {
                if (nbItems > 1) {
                  setState(() {
                    nbItems--;
                  });
                  widget.removeQty();
                }
              },
              child: SizedBox(
                width: 40,
                height: 32,
                child: OutlineButton(
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(13),
                  ),
                  child: Icon(Icons.remove),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                // if our item is less  then 10 then  it shows 01 02 like that
                nbItems.toString().padLeft(2, "0"),
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  setState(() {
                    nbItems++;
                  });
                  widget.addQty();
                });
              },
              child: SizedBox(
                width: 40,
                height: 32,
                child: OutlineButton(
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(13),
                  ),
                  child: Icon(Icons.add),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
