import 'package:flutter_app/models/cartItem_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/constants.dart';

class CartCard extends StatelessWidget {
  final CartItem cartItem;

  CartCard(this.cartItem);
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 100,
          child: Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: cartItem.product.color,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Image.asset(cartItem.product.image),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Text(cartItem.product.title),
            // Text(cartItem.size),
            // Text(cartItem.quantity.toString()),
            // Text(cartItem.price.toString()),
            Text(
              'x${cartItem.quantity} ${cartItem.product.title}',
              style: TextStyle(color: Colors.black, fontSize: 16),
              maxLines: 2,
            ),
            Text(
              'Size : ${cartItem.size}',
              style: TextStyle(color: Colors.black, fontSize: 12),
            ),
            SizedBox(height: 5),
            Text("\$${cartItem.price}",
                style: TextStyle(
                    fontWeight: FontWeight.w600, color: Colors.black)),
          ],
        ),
      ],
    );
  }
}
