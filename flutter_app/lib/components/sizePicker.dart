import 'package:flutter/material.dart';

class SizePicker extends StatefulWidget {
  final Function(String) changeSize;

  const SizePicker({Key key, this.changeSize}) : super(key: key);

  @override
  _SizePickerState createState() => _SizePickerState();
}

class _SizePickerState extends State<SizePicker> {
  String dropdownValue = '42';

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(
          'Size',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        DropdownButton(
            value: dropdownValue,
            items: <String>[
              '37',
              '38',
              '39',
              '40',
              '41',
              '42',
              '43',
              '44',
              '45',
              '46'
            ].map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
            onChanged: (String newValue) {
              setState(() {
                dropdownValue = newValue;
              });
              widget.changeSize(newValue);
            }),
      ],
    );
  }
}
