import 'package:flutter_app/models/cartItem_model.dart';
import 'package:flutter_app/models/cart_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/components/cartCard.dart';
import 'package:qrscan/qrscan.dart' as scanner;

class CartBody extends StatefulWidget {
  final Cart cart;
  final Function(int) removeItem;


  CartBody(this.cart, this.removeItem);
  @override
  _CartBodyState createState() => _CartBodyState(cart, removeItem);
}

class _CartBodyState extends State<CartBody> {
  final Cart cart;
  final Function(int) removeItem;

  Future _scan() async {
    String barcode = await scanner.scan();
    if (barcode == null) {
      print('nothing return.');
    } else {
      print(barcode);
    }
  }

  _CartBodyState(this.cart, this.removeItem);
  @override
  Widget build(BuildContext context) {
    return Column(
        children:[
          Expanded(
            child: ListView.builder(
            itemCount: cart.items.length,
            itemBuilder: (context, index) => Dismissible(
              key: Key(cart.items[index].id),
              direction: DismissDirection.endToStart,
              onDismissed: (direction) {
                setState(() {
                  removeItem(index);
                });
              },
              background: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Row(
                  children: [
                    Spacer(),
                    Icon(
                      Icons.delete,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
              child: Container(
                  margin: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black,
                            offset: Offset(0.0, 2.0),
                            blurRadius: 6.0)
                      ]),
                  child: CartCard(cart.items[index])),
            ),
          ),
          ),
          FlatButton(
            onPressed: () {_scan();},
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18)),
            color: Colors.red,
            child: Text("Proceed payment".toUpperCase(),
              style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.bold,
              color: Colors.white,),
            ),
          ),
        ]);
    // return Padding(
    //   padding: EdgeInsets.symmetric(horizontal: 10),
    //   child: ListView.builder(
    //       itemCount: cart.items.length,
    //       itemBuilder: (context, index) => Dismissible(
    //         key: Key(cart.items[index].id),
    //         direction: DismissDirection.endToStart,
    //         onDismissed: (direction) {
    //           setState(() {
    //             removeItem(index);
    //           });
    //         },
    //         background: Container(
    //           padding: EdgeInsets.symmetric(horizontal: 20),
    //           decoration: BoxDecoration(
    //             color: Colors.red,
    //             borderRadius: BorderRadius.circular(15),
    //           ),
    //           child: Row(
    //             children: [
    //               Spacer(),
    //               Icon(
    //                 Icons.delete,
    //                 color: Colors.white,
    //               ),
    //             ],
    //           ),
    //         ),
    //         child: Container(
    //             margin: EdgeInsets.all(5),
    //             decoration: BoxDecoration(
    //                 color: Colors.white,
    //                 borderRadius: BorderRadius.circular(20.0),
    //                 boxShadow: [
    //                   BoxShadow(
    //                       color: Colors.black,
    //                       offset: Offset(0.0, 2.0),
    //                       blurRadius: 6.0)
    //                 ]),
    //             child: CartCard(cart.items[index])),
    //       ),
    //     ),
        //   Positioned(
        //       bottom: 10.0,
        //       child: Container(
        //         width: MediaQuery.of(context).size.width - 10,
        //         height: 80.0,
        //         child: FlatButton(
        //           onPressed: () {},
        //           shape: RoundedRectangleBorder(
        //               borderRadius: BorderRadius.circular(18)),
        //           color: Colors.red,
        //           child: Text(
        //             "Proceed payment".toUpperCase(),
        //             style: TextStyle(
        //               fontSize: 17,
        //               fontWeight: FontWeight.bold,
        //               color: Colors.white,
        //             ),
        //           ),
        //         ),
        //       )
        //   ),
        // ]
  }
}


// FlatButton(
// onPressed: () {},
// shape: RoundedRectangleBorder(
// borderRadius: BorderRadius.circular(18)),
// color: Colors.red,
// child: Text(
// "Proceed payment".toUpperCase(),
// style: TextStyle(
// fontSize: 17,
// fontWeight: FontWeight.bold,
// color: Colors.white,
// ),
// ),
// ),