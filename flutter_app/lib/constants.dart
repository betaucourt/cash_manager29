import 'package:flutter/material.dart';

const metallicPurple = Color(0XFF463063);
const metallicRed = Color(0XFFE04031);
const metallicGreen = Color(0XFF419259);
const metallicOrange = Color(0XFFF46231);
